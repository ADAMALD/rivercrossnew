﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loadNextRiver : MonoBehaviour
{
    public GameObject river;
    Vector3 nextRiverPos = new Vector3();
    float newX;
    public static bool isSpawned = false;

    // Start is called before the first frame update
    void Start()
    {
        nextRiverPos = new Vector3(-13.3f, -51.29f, -2.6f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col) {
        Debug.Log("Next River");
        if (col.gameObject.name == "Player" && !isSpawned) {
            Instantiate(river, nextRiverPos, transform.rotation);
            newX = nextRiverPos.x - 39.2f;
            nextRiverPos = new Vector3(newX, -51.29f, -2.6f);
            isSpawned = true;
            river = GameObject.Find("RiverPrefab(1)");
        }
    }
}
