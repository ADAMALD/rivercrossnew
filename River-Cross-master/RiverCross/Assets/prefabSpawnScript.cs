﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class prefabSpawnScript : MonoBehaviour
{
    public int lilypadAmt;
    public GameObject lilypad;
    public bool ready = true;
    
    // Start is called before the first frame update



    void Start()
    {
        GameObject sp1 = gameObject.transform.GetChild(0).gameObject;
        GameObject sp2 = gameObject.transform.GetChild(1).gameObject;
        GameObject sp3 = gameObject.transform.GetChild(2).gameObject;
        GameObject sp4 = gameObject.transform.GetChild(3).gameObject;
        GameObject sp5 = gameObject.transform.GetChild(4).gameObject;
        GameObject[] sPoints = new GameObject[5];
    }

    IEnumerator SpawnPrefab()
    {
        ready = false;
        float ogXaxis = transform.position.x;
        float finalXaxis = transform.position.x + 20.6f;
        float spawnPointX = Random.Range(ogXaxis, finalXaxis);
        Vector3 spawnPosition = new Vector3(spawnPointX,transform.position.y,transform.position.z);
        Instantiate(lilypad, spawnPosition, transform.rotation);
        yield return new WaitForSeconds(0.8f);
        ready = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (ready)
        {
            StartCoroutine(SpawnPrefab());
        }

        
    }

}
